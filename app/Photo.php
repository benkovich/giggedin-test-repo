<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Conner\Tagging\Taggable;

class Photo extends Model
{
	use Taggable;
    
	protected $table = 'photos';
}
