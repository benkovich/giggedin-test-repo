@extends('layouts.default')
@section('title', 'Welcome')

@section('content')

<p>Welcome to the most amazing photo manager in the world.</p>
<p>I'm seriously not kidding... <a href="https://www.youtube.com/watch?v=-SK6gbhuEXQ" target="_blank">King Kong ain't got shit on this app.</a></p>

@if(session('success'))
<div class="alert alert-success">
	{{session('success')}}
</div>
@endif

<h1>Upload A Photo</h1>


<form action="{{ url('/') }}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="form-group">
		<label for="formGroupImageUpload">Photo:</label>
		<input type="file" name="photo" accept="image/*"  value="{{ old('image') }}" class="form-control">
		@if ($errors->has('photo'))
			<span class="text-danger">{{ $errors->first('photo') }}</span><br/>
		@endif
	</div>
	<div class="form-group">
		<label for="formGroupTags">Tags (comma separated):</label><br/> <!-- Fix CSS quirk to enforce new line -->
		<input data-role="tagsinput" type="text" name="tags" value="{{ old('tags') }}" class="form-control"/>
		@if ($errors->has('tags'))
			<br/><span class="text-danger">{{ $errors->first('tags') }}</span>
		@endif
	</div>
	<button class="btn btn-success btn-submit">Upload</button>
</form>

<h1>Tags</h1>

<p>Click any of the tags below to view uploaded images</p>

@forelse($tags as $tag)
	<a href="{{ url('/tags/'.$tag['slug']) }}"><button type="button" class="btn btn-primary">{{ $tag['name'] }} <span class="badge">{{ $tag['count'] }}</span></button></a>
@empty
	<em>You haven't assigned any tags yet. Upload a photo and tags will appear here.</em>
@endforelse
	
@endsection

