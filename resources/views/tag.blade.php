@extends('layouts.default')
@section('title', 'Tag : '.$tag['name'])

@section('content')

<p>This is the tag page for : <span class="label label-primary">{{ $tag['name'] }}</span></p>
<p>The number of photos assigned to this tag is : <span class="badge">{{ $tag['count'] }}</span></p>
<p><a href="{{ URL::to('/') }}">Go Back</a></p>

<table class="table">
	<thead>
		<tr>
		<th scope="col">ID</th>
		<th scope="col">Image</th>
		<th scope="col">Uploaded</th>
		</tr>
	</thead>
	<tbody>
		@foreach($photos as $photo)
		<tr>
		<td scope="row">{{ $photo['id'] }}</td>
		<td><a href="{{ $photo['path'] }}" target="_blank"><img src="{{ $photo['path'] }}" class="img-thumbnail" width="100" height="100" /></a></td>
		<td>{{ $photo['created_at'] }}</td>
		</tr>
		@endforeach
	</tbody>
</table>



@endsection
