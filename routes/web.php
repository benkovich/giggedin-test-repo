<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PhotoController@index'); // Displays the homepage
Route::post('/', 'PhotoController@store'); // Uploads the photo
Route::get('tags/{slug}', 'PhotoController@tags'); // Shows all photos associated with tag $slug

